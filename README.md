# Documentation Jour 3
Lors du déploiement du cluster AKS, il n'était pas présent dans la liste, je l'ai rajouter vi cette commande :
`az aks get-credentials --name aks-j-brenet-240205 --resource-group rg-j-brenet-240205`


# App Node.js pour formation CI/CD : galerie d'images

Ce projet est à but éducatif uniquement, et peut servir à démontrer la configuration et l'utilisation de pipelines CI/CD avec GitLab, Jenkins, GitHub Actions, etc.

C'est une application Node.js, construite sur le framework [Express](https://expressjs.com), implémentant une architecture traditionnelle : génération de pages HTML.

## Pré-requis

* **Version de Node.js** : comme, au moment de la rédaction, Node.js 20 est en phase de support à long terme actif, vous devriez l'avoir installé (ou au moins la version 18). Vous pouvez vérifier quelle version est installée sur votre ordinateur en ouvrant un terminal et en exécutant `node -v`. Si vous ne l'avez pas ou si votre version est trop ancienne, vous pouvez l'installer en utilisant [nvm](https://github.com/nvm-sh/nvm) (Linux, macOS) ou [nvm-windows](https://github.com/coreybutler/nvm-windows).
* **Yarn** : [Yarn](https://yarnpkg.com) est une alternative populaire à NPM, souvent plus performante. Nous l'utiliserons pour la gestion des dépendances. Vous pouvez vérifier s'il est installé en exécutant `yarn -v`. S'il ne l'est pas, exécutez simplement `npm i -g yarn`.

## Installation

**:warning: [TODO] remplacer l'origine du repo**

* Allez dans votre "répertoire de projets" habituel, clonez le projet : `git clone https://gitlab.com/bhubert/ipi-cicd-sample-testing`
* `cd ipi-cicd-node-ts-ecommerce`
* Exécutez `yarn`

## Lancement

Lancez l'application en mode développement via la commande `yarn dev`.
